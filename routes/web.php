<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$register = function() use ($router) {
    $router->post('/auth/token/', 'AuthController@login');

    $router->group(['middleware' => 'auth'], function() use ($router) {
        $router->get("/users/me/", 'UsersController@me');

        $router->group(['prefix' => '/tickets/'], function () use ($router) {
            $router->get('/', 'TicketsController@list');
            $router->post('/', 'TicketsController@create');

            $router->group(['prefix' => '/{id}/'], function () use ($router) {
                $router->get('/', 'TicketsController@read');
                $router->patch('/', 'TicketsController@update');
                $router->delete('/', 'TicketsController@delete');
            });
        });

        $router->group(['prefix' => '/bugs/{id}/'], function () use ($router) {
            $router->patch('/', 'BugsController@update');
            $router->delete('/', 'BugsController@delete');
        });

        $router->get("/modules/", 'ModulesController@list');
        $router->get('/programming-languages/', 'ProgrammingLanguagesController@list');

        $router->group(['prefix' => '/patches'], function () use ($router) {
            $router->get('/', 'PatchesController@list');
            $router->post('/', 'PatchesController@create');

            $router->group(['prefix' => '/{id}/'], function () use ($router) {
                $router->get('/', 'PatchesController@read');
                $router->patch('/', 'PatchesController@update');
                $router->delete('/', 'PatchesController@delete');
            });
        });

        $router->group(['middleware' => 'admin'], function () use ($router) {
            $router->group(['prefix' => '/users'], function () use ($router) {
                $router->get("/", 'UsersController@list');
                $router->post("/", 'UsersController@create');
                $router->patch("/{id}/", 'UsersController@update');
            });

            $router->group(['prefix' => '/modules'], function () use ($router) {
                $router->post("/", 'ModulesController@create');
                $router->patch("/{id}/", 'ModulesController@update');
                $router->delete("/{id}/", 'ModulesController@delete');
            });

            $router->group(['prefix' => '/programming-languages'], function () use ($router) {
                $router->post('/', 'ProgrammingLanguagesController@create');
                $router->patch('/{id}/', 'ProgrammingLanguagesController@update');
                $router->delete('/{id}/', 'ProgrammingLanguagesController@delete');
            });
        });


    });
};

if (env('APP_URL_PREFIX'))
    $router->group(['prefix' => env('APP_URL_PREFIX')], $register);
else
    $register();
