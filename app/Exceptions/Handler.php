<?php

namespace App\Exceptions;

use App\Util\ResponseFactory;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        $code = $this->getCode($exception);
        return $code == JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            ? ResponseFactory::error($this->getMessage($exception), $code)
            : ResponseFactory::fail(['message' => $this->getMessage($exception)], $code);
    }

    private function getMessage(Exception $exception)
    {
        $cond = app()->environment('local') ||
            $exception instanceof HttpException;

        return !$cond ? "Something went wrong!" : $exception->getMessage();
    }

    private function getCode(Exception $exception)
    {
        if ($exception instanceof HttpException)
            return $exception->getStatusCode();
        else if ($exception instanceof ModelNotFoundException)
            return JsonResponse::HTTP_NOT_FOUND;
        else if ($exception instanceof ValidationException)
            return JsonResponse::HTTP_UNPROCESSABLE_ENTITY;

        return JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
    }
}
