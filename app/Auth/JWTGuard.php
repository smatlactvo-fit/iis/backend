<?php

namespace App\Auth;

use App\Util\JWT;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class JWTGuard implements \Illuminate\Contracts\Auth\Guard
{
    use GuardHelpers;

    /** @var Request */
    private $request;

    /**
     * JWTGuard constructor.
     *
     * @param UserProvider $provider
     * @param Request $request
     */
    public function __construct(UserProvider $provider, Request $request)
    {
        $this->provider = $provider;
        $this->request = $request;
    }

    /**
     * @inheritDoc
     */
    public function authenticate()
    {
        if (! $this->isValid($user = $this->user())) {
            return $user;
        }

        throw new AuthenticationException;
    }

    /**
     * @inheritDoc
     */
    public function check()
    {
        return $this->isValid($this->user());
    }

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param array $credentials
     *
     * @return bool
     */
    public function attempt(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);
        if($this->hasValidCredentials($user, $credentials)) {
            $this->user = $user;

            return true;
        };

        return false;
    }

    /**
     * @inheritDoc
     */
    public function validate(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);
        return  $this->hasValidCredentials($user, $credentials);
    }

    /**
     * @inheritDoc
     */
    public function user()
    {
        if (! is_null($this->user)) {
            return $this->user;
        }

        if(! $this->request->bearerToken()) return null;

        $decoded = JWT::decode($this->request->bearerToken());
        return ($this->user = $this->provider->retrieveById($decoded->id));
    }

    /**
     * Check user validity.
     *
     * @param       $user
     *
     * @return bool
     */
    protected function isValid($user)
    {
        return ! is_null($user) && $user->active;
    }

    /**
     * Check user credentials.
     *
     * @param       $user
     * @param array $credentials
     *
     * @return bool
     */
    protected function hasValidCredentials($user, array $credentials)
    {
        return ! is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }
}
