<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{
    protected $fillable = ['created_by'];
    protected $hidden = ['updated_at'];
    protected $appends = ['watched'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function getWatchedAttribute(User $user = null)
    {
        $user = $user ?: Auth::user();
        return $this->watchedBy()->where('user', '=', $user->id)->count() > 0;
    }

    public function watchedBy()
    {
        return $this->belongsToMany(User::class, 'users_watches_tickets', 'ticket', 'user');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function bugs()
    {
        return $this->hasMany(Bug::class, 'ticket', 'id');
    }
}