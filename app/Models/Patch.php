<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Patch extends Model
{
    const STATES = [self::RELEASED, self::APPROVED, self::WIP];
    const RELEASED = 'released';
    const APPROVED = 'approved';
    const WIP = 'WIP';

    protected $fillable = ['approved_at', 'approved_by', 'released_at', 'released_by'];
    protected $hidden = ['create_at', 'updated_at'];

    protected $appends = [
        'state'
    ];

    protected $dates = [
        'approved_at',
        'released_at',
        'created_at',
        'updated_at'
    ];

    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function releasedBy()
    {
        return $this->belongsTo(User::class, 'released_by', 'id');
    }

    public function isApproved()
    {
        return !!$this->approved_at && !!$this->approved_by;
    }

    public function isReleased()
    {
        return !!$this->released_at && !!$this->released_by;
    }

    public function getStateAttribute()
    {
        return $this->isReleased() ? self::RELEASED
            : ($this->isApproved() ? self::APPROVED
                : self::WIP);
    }

    public function bugs()
    {
        return $this->hasMany(Bug::class, 'patch', 'id');
    }
}