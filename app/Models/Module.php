<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['lang', 'name', 'admin'];
    protected $hidden = ['created_at', 'updated_at'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin', 'id');
    }

    public function lang()
    {
        return $this->belongsTo(ProgrammingLanguage::class, 'lang', 'id');
    }

    public function bugs()
    {
        return $this->hasMany(Bug::class, 'module', 'id');
    }
}