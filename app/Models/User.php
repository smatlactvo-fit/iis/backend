<?php

namespace App\Models;


use App\Util\Constants;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $fillable = ['role', 'username', 'password', 'birthday', 'active'];
    protected $hidden = ['password', 'created_at', 'updated_at'];
    protected $casts = [
        'active' => 'boolean',
    ];
    protected $attributes = [
        'active' => true,
    ];

    protected $dates = [
        'birthday',
        'created_at',
        'updated_at'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setBirthdayAttribute($value) {
        $this->attributes['birthday'] = Carbon::createFromFormat(Constants::DATE_TIME_FORMAT, $value);
    }

    public function langs() {
        return $this->belongsToMany(ProgrammingLanguage::class, 'users_langs_map', 'user', 'programming_language');
    }
}