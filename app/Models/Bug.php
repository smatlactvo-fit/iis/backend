<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Bug extends Model
{
    protected $fillable = ['description', 'priority', 'module'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['resolved'];
    protected $dates = ['created_at', 'updated_at'];


    public function module()
    {
        return $this->belongsTo(Module::class, 'module', 'id');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket', 'id');
    }

    public function patch()
    {
        return $this->belongsTo(Patch::class, 'patch', 'id');
    }

    public function getResolvedAttribute()
    {
        /** @var Patch $patch */
        $patch = $this->patch()->first();
        return $patch != null ? $patch->isApproved() : false;
    }
}