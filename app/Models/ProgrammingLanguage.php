<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProgrammingLanguage extends Model
{
    protected $fillable = ['name'];
    protected $hidden = ['created_at', 'updated_at', 'pivot'];

    public function modules()
    {
        return $this->hasMany(Module::class, 'id', 'module');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_langs_map', 'programming_language', 'user');
    }
}