<?php


namespace App\Util;


class Constants
{
    const DATE_TIME_FORMAT = "Y-m-d\TH:i:s.uO";
}