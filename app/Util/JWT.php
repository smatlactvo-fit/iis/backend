<?php

namespace App\Util;


class JWT
{
    public static function decode(?string $jwt)
    {
        return \Firebase\JWT\JWT::decode($jwt, env("APP_KEY"), ['HS512', 'HS256']);
    }

    public static function encode(array $payload)
    {
        $payload = ['exp' => time() + env("JWT_EXPIRATION", "1200")] + $payload;
        return \Firebase\JWT\JWT::encode($payload, env("APP_KEY"), env("JWT_ALG", "HS512"));
    }
}
