<?php


namespace App\Util;


class Functions
{
    public static function rethrow(callable $callback, $exception)
    {
        try {
            $callback();
        } catch (\Throwable $e) {
            throw new $exception($e);
        }
    }
}