<?php


namespace App\Http\Controllers;


use App\Models\User;
use App\Services\UserService;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /** @var UserService */
    private $service;

    public function __construct(UserService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function me()
    {
        $user = Auth::user();
        return ResponseFactory::success(compact('user'));
    }

    public function list() {
        $users = User::all()->load('langs');
        return ResponseFactory::success(compact('users'));
    }

    public function create(Request $request)
    {
        $validated = $this->validate($request, [
            'username' => 'required|max:30',
            'role' => 'required|max:15|in:admin,programmer,user',
            'password' => 'required|max:100',
            'birthday' => 'required|date',
            'langs' => 'present|array',
            'langs.*' => 'numeric|exists:programming_languages,id'
        ]);

        $user = $this->service->create($validated);
        $user->load('langs');
        return ResponseFactory::success(compact('user'));
    }

    public function update(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'username' => 'sometimes|max:30|required',
            'role' => 'sometimes|max:15|in:admin,programmer,user',
            'birthday' => 'sometimes|date',
            'active' => 'sometimes|bool',
            'langs' => 'sometimes|present|array',
            'langs.*' => 'numeric|exists:programming_languages,id'
        ]);


        $id = $id == 'me' ? Auth::user()->id : $id;
        $user = $this->service->update($id, $validated);
        $user->load('langs');
        return ResponseFactory::success(compact('user'));
    }
}