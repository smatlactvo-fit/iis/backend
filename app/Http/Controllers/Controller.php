<?php

namespace App\Http\Controllers;

use App\Util\ResponseFactory;
use Laravel\Lumen\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
    /**
     * Controller constructor.
     */
    public function __construct()
    {
        self::buildResponseUsing(function(Request $request, array $errors) {
            return ResponseFactory::fail($errors);
        });
    }
}
