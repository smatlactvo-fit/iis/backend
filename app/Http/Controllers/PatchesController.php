<?php


namespace App\Http\Controllers;


use App\Models\Patch;
use App\Services\PatchService;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;

class PatchesController extends Controller
{
    /** @var PatchService */
    private $service;

    /**
     * PatchesController constructor.
     * @param PatchService $service
     */
    public function __construct(PatchService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function list()
    {
        $patches = Patch::all()->load('bugs.module', 'approvedBy', 'releasedBy');
        return ResponseFactory::success(compact('patches'));
    }

    public function read(int $id)
    {
        /** @var Patch $patch */
        $patch = Patch::findOrFail($id);
        $patch->load('bugs.module', 'approvedBy', 'releasedBy');
        return ResponseFactory::success(compact('patch'));
    }

    public function create(Request $request)
    {
        $validated = $this->validate($request, [
            'bugs' => 'required|array',
            'bugs.*' => 'sometimes|numeric|exists:bugs,id',
        ]);

        $patch = $this->service->create($validated);
        $patch->load('bugs.module', 'approvedBy', 'releasedBy');
        return ResponseFactory::success(compact('patch'));
    }

    public function update(Request $request, int $id)
    {
        if ($request->has('approve'))
            $patch = $this->service->approve($id);
        else if ($request->has('release'))
            $patch = $this->service->release($id);

        $patch = $patch ?? Patch::find($id);
        $patch->load('bugs.module', 'approvedBy', 'releasedBy');
        return ResponseFactory::success(compact('patch'));
    }

    public function delete(int $id)
    {
        $this->service->delete($id);
        return ResponseFactory::success([
            'message' => 'Successfully deleted'
        ]);
    }
}