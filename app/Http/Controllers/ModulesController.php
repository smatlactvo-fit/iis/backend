<?php


namespace App\Http\Controllers;


use App\Models\Module;
use App\Services\ModuleService;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    /** @var ModuleService */
    private $service;

    /**
     * ModulesController constructor.
     * @param ModuleService $service
     */
    public function __construct(ModuleService $service)
    {
        parent::__construct();
        $this->service = $service;
    }


    public function list()
    {
        $modules = Module::all()->load('lang', 'admin');
        return ResponseFactory::success(compact('modules'));
    }

    public function create(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required|max:30|unique:modules,name',
            'lang' => 'required|numeric|exists:programming_languages,id',
            'admin' => 'required|numeric|exists:users,id'
        ]);

        $module = $this->service->create($validated);
        $module->load('lang', 'admin');
        return ResponseFactory::success(compact('module'));
    }

    public function update(Request $request, int $id)
    {
        $validated = $this->validate($request, [
            'name' => "sometimes|max:30|unique:modules,name,$id",
            'lang' => 'sometimes|numeric|exists:programming_languages,id',
            'admin' => 'sometimes|numeric|exists:users,id'
        ]);

        $module = $this->service->update($id, $validated);
        $module->load('lang', 'admin');
        return ResponseFactory::success(compact('module'));
    }

    public function delete(int $id)
    {
        $this->service->delete($id);
        return ResponseFactory::success([
            'message' => 'Successfully removed!'
        ]);
    }

}