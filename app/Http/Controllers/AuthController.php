<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Util\JWT;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /** @var UserService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = $this->service->login($request->input('username'), $request->input('password'));

        return ResponseFactory::success([
            'token' => JWT::encode(['id' => $user->id]),
        ]);
    }
}
