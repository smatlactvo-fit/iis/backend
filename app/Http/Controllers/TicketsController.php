<?php


namespace App\Http\Controllers;


use App\Models\Ticket;
use App\Services\TicketService;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    /** @var TicketService */
    private $service;

    public function __construct(TicketService $service)
    {
        parent::__construct();
        $this->service = $service;
    }


    public function list()
    {
        $tickets = Ticket::all()->load('bugs.module', 'createdBy');
        return ResponseFactory::success(compact('tickets'));
    }

    public function read(int $id)
    {
        /** @var Ticket $ticket */
        $ticket = Ticket::findOrFail($id);
        $ticket->load('bugs.module', 'createdBy');
        return ResponseFactory::success(compact('ticket'));
    }

    public function create(Request $request)
    {
        $validated = $this->validate($request, [
            'bugs' => 'required|array',
            'bugs.*' => 'sometimes',
            'bugs.*.description' => 'required',
            'bugs.*.priority' => 'required|numeric',
            'bugs.*.module' => 'required|numeric|exists:modules,id',
        ]);

        $ticket = $this->service->create($validated);
        $ticket->load('bugs.module', 'createdBy');
        return ResponseFactory::success(compact('ticket'));
    }

    public function update(Request $request, int $id)
    {
        $this->validate($request, [
            'watched' => 'sometimes|boolean'
        ]);

        $ticket = $this->service->update($id, $request->input());
        $ticket->load('bugs.module', 'createdBy');
        return ResponseFactory::success(compact('ticket'));
    }

    public function delete(int $id)
    {
        $this->service->delete($id);
        return ResponseFactory::success([
            'message' => 'Successfully deleted'
        ]);
    }
}