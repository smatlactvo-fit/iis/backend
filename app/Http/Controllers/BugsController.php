<?php


namespace App\Http\Controllers;


use App\Services\BugService;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;

class BugsController extends Controller
{
    /** @var BugService */
    private $service;

    public function __construct(BugService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function update(Request $request, int $id)
    {
        $validated = $this->validate($request, [
            'priority' => 'sometimes|int',
            'description' => 'sometimes|max:255',
            'module' => 'sometimes|numeric|exists:modules,id'
        ]);

        $bug = $this->service->update($id, $validated);
        return ResponseFactory::success(compact('bug'));
    }

    public function delete(int $id)
    {
        $this->service->delete($id);
        return ResponseFactory::success([
            'message' => 'Successfully deleted'
        ]);
    }
}