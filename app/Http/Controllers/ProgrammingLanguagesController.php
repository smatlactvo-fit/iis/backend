<?php


namespace App\Http\Controllers;


use App\Models\ProgrammingLanguage;
use App\Services\ProgrammingLanguageService;
use App\Util\ResponseFactory;
use Illuminate\Http\Request;

class ProgrammingLanguagesController extends Controller
{
    /** @var ProgrammingLanguageService */
    private $service;

    public function __construct(ProgrammingLanguageService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function list() {
        $langs = ProgrammingLanguage::all();
        return ResponseFactory::success(compact('langs'));
    }

    public function create(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required|max:30|unique:programming_languages,name',
        ]);

        $lang = $this->service->create($validated);
        return ResponseFactory::success(compact('lang'));
    }

    public function update(Request $request, int $id)
    {
        $validated = $this->validate($request, [
            'name' => 'sometimes|max:30|unique:programming_languages,name',
        ]);

        $lang = $this->service->update($id, $validated);
        return ResponseFactory::success(compact('lang'));
    }

    public function delete(int $id)
    {
        $this->service->delete($id);
        return ResponseFactory::success(['message' => 'Successfully deleted!']);
    }
}