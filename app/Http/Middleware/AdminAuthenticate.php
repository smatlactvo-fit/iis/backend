<?php


namespace App\Http\Middleware;


use App\Util\ResponseFactory;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticate
{
    /** @var \Illuminate\Contracts\Auth\Factory|Auth */
    protected $auth;

    const ADMIN_ROLE = 'admin';

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if (Auth::user()->role != self::ADMIN_ROLE) {
            return ResponseFactory::fail(JsonResponse::$statusTexts[JsonResponse::HTTP_UNAUTHORIZED], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}