<?php


namespace App\Services;


use App\Models\Ticket;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class TicketService
{
    public function create(array $attributes)
    {
        $ticket = new Ticket(Arr::except($attributes, 'bugs'));
        $ticket->createdBy()->associate(Auth::user());
        $ticket->save();

        $ticket->bugs()->createMany($attributes['bugs']);

        $ticket->watchedBy()->attach(Auth::user());

        return $ticket;
    }

    public function update(int $id, array $attributes)
    {
        /** @var Ticket $ticket */
        $ticket = Ticket::findOrFail($id);

        if (($attributes['watched'] ?? false) && !$ticket->watched)
            $ticket->watchedBy()->attach(Auth::user());
        if (!($attributes['watched'] ?? true) && $ticket->watched)
            $ticket->watchedBy()->detach(Auth::user());


        return $ticket;
    }

    public function delete(int $id)
    {
        /** @var Ticket $ticket */
        $ticket = Ticket::findOrFail($id);

        $ticket->bugs()->whereNull('patch')->delete();
        $ticket->bugs()->update(['ticket' => null]);

        $ticket->watchedBy()->sync([]);

        $ticket->delete();

        return $ticket;
    }

}