<?php


namespace App\Services;


use App\Models\ProgrammingLanguage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProgrammingLanguageService
{
    public function create(array $attributes)
    {
        $lang = new ProgrammingLanguage($attributes);
        $lang->save();

        return $lang;
    }

    public function update(int $id, array $attributes)
    {
        /** @var ProgrammingLanguage $lang */
        $lang = ProgrammingLanguage::findOrFail($id);
        $lang->fill($attributes);
        $lang->save();

        return $lang;
    }

    public function delete(int $id)
    {
        /** @var ProgrammingLanguage $lang */
        $lang = ProgrammingLanguage::findOrFail($id);

        if ($lang->modules()->count() > 0)
            throw new HttpException(409, "There are some modules that depends on this language!");
        if ($lang->users()->count() > 0)
            throw new HttpException(409, "There are some users that depends on this language!");

        $lang->delete();
    }
}