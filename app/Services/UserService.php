<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserService
{
    /**
     * @param string $username
     * @param string $password
     * @return Authenticatable|User
     */
    public function login(string $username, string $password) : Authenticatable
    {
        if(!Auth::attempt(compact('username', 'password'))) {
            abort(JsonResponse::HTTP_UNAUTHORIZED, JsonResponse::$statusTexts[JsonResponse::HTTP_UNAUTHORIZED]);
        }

        if (!Auth::check()) {
            abort(JsonResponse::HTTP_CONFLICT, JSonResponse::$statusTexts[JsonResponse::HTTP_CONFLICT]);
        }

        return Auth::user();
    }

    public function create(array $attributes)
    {
        $user = new User($attributes);
        $user->save();

        // TODO: we should let know, when this fails.
        $user->langs()->sync($attributes['langs']);

        return $user;
    }

    public function update(int $id, array $attributes)
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        $user->fill($attributes);
        $user->save();

        // TODO: we should let know, when this fails.
        $user->langs()->sync($attributes['langs']);

        return $user;
    }
}