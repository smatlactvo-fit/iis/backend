<?php


namespace App\Services;


use App\Models\Bug;
use App\Models\Patch;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PatchService
{
    public function create(array $attributes)
    {
        $assigned = Bug::query()->whereIn('id', $attributes['bugs'])->whereNotNull('patch')->exists();
        if ($assigned != null) throw new HttpException(424, 'Some bugs are already at patch process!');

        $patch = new Patch();
        $patch->save();

        $bugs = Bug::query()->whereIn('id', $attributes['bugs'])->whereNull('patch')->get();
        $patch->bugs()->saveMany($bugs);

        return $patch;
    }

    public function approve(int $id)
    {
        /** @var Patch $patch */
        $patch = Patch::findOrFail($id);

        if ($patch->isReleased()) throw new HttpException(409, "Already realesed!");
        if ($patch->isApproved()) throw new HttpException(409, "Already approved!");

        $patch->approvedBy()->associate(Auth::user());
        $patch->approved_at = Carbon::now();

        $patch->save();
        return $patch;
    }

    public function release(int $id)
    {
        /** @var Patch $patch */
        $patch = Patch::find($id);

        if (!$patch->isApproved()) throw new HttpException(409, "Need to be approved first!");
        if ($patch->isReleased()) throw new HttpException(409, "Already realesed!");

        $patch->releasedBy()->associate(Auth::user());
        $patch->released_at = Carbon::now();

        $patch->save();
        return $patch;
    }

    public function delete(int $id)
    {
        /** @var Patch $patch */
        $patch = Patch::find($id);

        if ($patch->isReleased()) throw new HttpException(409, "Already released!");
        if ($patch->isApproved()) throw new HttpException(409, "Already approved!");

        $patch->bugs()->whereNull('ticket')->delete();
        $patch->bugs()->update(['patch' => null]);

        $patch->delete();
    }
}