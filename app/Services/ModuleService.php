<?php


namespace App\Services;


use App\Models\Module;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ModuleService
{
    public function create(array $attributes)
    {
        $module = new Module($attributes);
        $module->lang()->associate($attributes['lang']);
        $module->save();

        return $module;
    }

    public function update(int $id, array $attributes)
    {
        /** @var Module $module */
        $module = Module::findorFail($id);
        $module->fill($attributes);
        $module->save();

        return $module;
    }

    public function delete(int $id)
    {
        /** @var Module $module */
        $module = Module::findorFail($id);

        if ($module->bugs()->count() > 0)
            throw new HttpException(409, "There are components that depends on this module!");

        $module->delete();
    }
}