<?php


namespace App\Services;


use App\Models\Bug;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BugService
{

    public function update(int $bugId, array $attributes)
    {
        /** @var Bug $bug */
        $bug = Bug::findOrFail($bugId);

        if ($bug->patch()->exists())
            throw new HttpException(409, "This bug cannot be modified due to its in patch process!");

        $bug->fill($attributes);
        $bug->save();

        return $bug;
    }

    public function delete(int $bugId)
    {
        /** @var Bug $bug */
        $bug = Bug::findOrFail($bugId);

        if ($bug->patch()->exists())
            throw new HttpException(409, "This bug cannot be modified due to its in patch process!");

        $bug->delete();
    }
}