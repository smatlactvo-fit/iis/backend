<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->name,
        'role' => $faker->randomElement(['user', 'programmer', 'admin']),
        'password' => 'secret',
        'birthday' => $faker->dateTimeInInterval('-30 years', '+5 months')->format(\App\Util\Constants::DATE_TIME_FORMAT),
        'active' => true
    ];
});

$factory->define(App\Models\ProgrammingLanguage::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->languageCode // this is example
    ];
});

$factory->define(App\Models\Module::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->domainName, // this is example
        'lang' => $faker->randomElement(\App\Models\ProgrammingLanguage::all()->pluck('id')->toArray()),
        'admin' => $faker->randomElement(\App\Models\User::query()->where('role', '=', 'programmer')->pluck('id')->toArray()),
    ];
});

$factory->define(App\Models\Ticket::class, function (Faker\Generator $faker) {
    return [
        'created_by' => $faker->randomElement(\App\Models\User::all()->pluck('id')->toArray())
    ];
});

$factory->define(App\Models\Bug::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->paragraph(5),
        'priority' => $faker->numberBetween(0, 5),
        'module' => $faker->randomElement(\App\Models\Module::all()->pluck('id')->toArray())
    ];
});

$factory->define(App\Models\Patch::class, function (Faker\Generator $faker) {
    $state = $faker->randomElement(\App\Models\Patch::STATES);

    $entity = [];

    if ($state != \App\Models\Patch::WIP) {
        $entity += [
            'approved_by' => \App\Models\User::all()->random(),
            'approved_at' => $faker->dateTime
        ];
    }

    if ($state == \App\Models\Patch::RELEASED) {
        $entity += [
            'released_by' => \App\Models\User::all()->random(),
            'released_at' => $faker->dateTime
        ];
    }

    return $entity;
});
