<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patches', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('approved_at')->nullable();
            $table->unsignedInteger('approved_by')->nullable();
            $table->foreign('approved_by')->references('id')->on('users');

            $table->dateTime('released_at')->nullable();
            $table->unsignedInteger('released_by')->nullable();
            $table->foreign('released_by')->references('id')->on('users');

            $table->timestamps();
        });

        Schema::table('bugs', function (Blueprint $table) {
            $table->unsignedInteger('patch')->nullable();
            $table->foreign('patch')->references('id')->on('patches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bugs', function (Blueprint $table) {
            $table->dropForeign(['patch']);
            $table->dropColumn('patch');
        });
        Schema::dropIfExists('patches');
    }
}
