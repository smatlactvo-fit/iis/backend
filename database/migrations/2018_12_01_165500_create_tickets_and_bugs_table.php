<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsAndBugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by');

            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamps();
        });


        Schema::create('bugs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('priority');

            $table->unsignedInteger('ticket')->nullable();
            $table->foreign('ticket')->references('id')->on('tickets');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bugs');
        Schema::dropIfExists('tickets');
    }
}
