<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersWatchesTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_watches_tickets', function (Blueprint $table) {
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users');

            $table->unsignedInteger('ticket');
            $table->foreign('ticket')->references('id')->on('tickets');

            $table->primary(['user', 'ticket']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_watches_tickets');
    }
}
