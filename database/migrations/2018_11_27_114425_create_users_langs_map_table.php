<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersLangsMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_langs_map', function (Blueprint $table) {
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users');

            $table->unsignedInteger('programming_language');
            $table->foreign('programming_language')->references('id')->on('programming_languages');

            $table->primary(['user', 'programming_language']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_langs_map');
    }
}
