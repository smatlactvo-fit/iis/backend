<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\User::class)->create([
            'role' => 'admin',
            'username' => 'iisproject',
            'password' => 'secret'
        ]);
        factory(App\Models\User::class, 20)->create();
    }
}
