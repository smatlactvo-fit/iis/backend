<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProgrammingLanguagesTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(TicketsAndBugsSeeder::class);
        $this->call(UserWatchesTicketsSeeder::class);
        $this->call(PatchSeeder::class);
    }
}
