<?php

use App\Models\Patch;
use Illuminate\Database\Seeder;

class PatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(6, 9) as $i) {
            /** @var Patch $patch */
            $patch = factory(Patch::class)->create();
            $patch->bugs()->saveMany(\App\Models\Bug::query()->whereNull('patch')->get()->random(rand(9, 13)));
        }
    }

}