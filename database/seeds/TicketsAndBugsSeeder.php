<?php

use Illuminate\Database\Seeder;

class TicketsAndBugsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 15) as $i) {
            $ticket = factory(\App\Models\Ticket::class)->create();
            factory(\App\Models\Bug::class, rand(7, 20))->create(['ticket' => $ticket->id]);
        }
    }
}
