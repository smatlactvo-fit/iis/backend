<?php

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserWatchesTicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $res = [];

        $users = User::all();
        foreach ($users as $user) {
            $tickets = Ticket::all()->random(4);
            foreach ($tickets as $ticket) {
                $res[] = [
                    'user' => $user->id,
                    'ticket' => $ticket->id,
                ];
            }
        }

        DB::table('users_watches_tickets')->insert($res);
    }
}
